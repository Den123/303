root@den:~# docker run -t -i debian:jessie bash
Unable to find image 'debian:jessie' locally
jessie: Pulling from library/debian
afee0e7e1857: Pull complete
Digest: sha256:d54da4e38a30a690017856dc3b67e53bf3a3094dd693006b5d00408ba1d90d50
Status: Downloaded newer image for debian:jessie
root@218cc62e13a0:/# cat /etc/os-release
PRETTY_NAME="Debian GNU/Linux 8 (jessie)"
NAME="Debian GNU/Linux"
VERSION_ID="8"
VERSION="8 (jessie)"
ID=debian
HOME_URL="http://www.debian.org/"
SUPPORT_URL="http://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
